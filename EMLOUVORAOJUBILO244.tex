\chapterspecial{Em Louvor ao Júbilo}{}{}
 \epigraph{A partir das condições do mundo moderno onde somos ameaçados não apenas
pelo não"-ser"-coisa, mas também pelo não"-ser"-alguém, coloca"-se a questão:
por que deveria haver alguém e não, pelo contrário, ninguém? Essas
perguntas podem parecer niilistas, mas não são. Na situação objetiva do
niilismo, onde o não"-ser"-coisa e o não"-ser"-alguém ameaçam destruir o
mundo, são questões antiniilistas.
\versal{HANNAH} \versal{ARENDT}}{\versal{HANNAH} \versal{ARENDT}} 

\epigraph{Alfabeto Enquanto eu estava no frio das aproximações da morte, olhei
como pela última vez os seres, profundamente. Ao contato mortal desse
olhar de gelo, tudo que não era essencial desapareceu. Entretanto eu os
folheava, querendo reter deles alguma coisa que nem mesmo o Morto
pudesse desfazer. Eles minguaram e encontraram"-se enfim reduzidos a uma
espécie de alfabeto, mas um alfabeto que teria servido em outro mundo,
em qualquer mundo. Assim, eu me aliviei do medo de arrancarem de mim
todo o universo em que eu tinha vivido. Reassegurado por essa visão, eu
o contemplava invicto, quando o sangue, com satisfação, voltou nas
minhas artérias e veias; lentamente eu escalei a encosta aberta da vida.
\versal{HENRI} \versal{MICHAUX}}{\versal{HENRI} \versal{MICHAUX}} 

\begin{flushright}\emph{Para Gilberto Safra, parteiro dos dois risos.}\end{flushright}


A vida de um homem sobre a terra comporta dois momentos de sorriso
verdadeiro. Se o primeiro momento acontece quando se passa do dizer
autobiográfico de um eu condicionado pelas ideias de formação e por
éticas universalizantes para o dizer do si"-mesmo a partir das marcas
reais e dos acontecimentos efetivos, o segundo acontece quando o homem
se desmancha e ri das próprias marcas. É~o sorriso taoísta da
serenidade\footnote{Este texto, uma experiência com o pensamento de Nietzsche, tratará
apenas da parte biográfica da existência e da passagem do niriguém ao
alguém. Já o salto homeopático do alguém ao grande ninguém, não será
tematizado aqui. Ele envolve um mergulho em Heidegger e Chuang Tzu.} .

O primeiro sorriso pode ser chamado de sorriso da singularização. Alguns
poucos conhecem a exuberância desse sorriso, pois a grande maioria dos
homens passa a vida inteira submetida a ideais ou ajustando o vivido a
medidas fictícias e representadas. Mesmo que um homem não caiba inteira
e perfeitamente no formato de alguma caixa do ideal, ele tenta fingir
que coube e esquecer ou encobrir a parte que ficou de fora. Se está
muito apertado na caixa e uma ferida começa a se formar, então ele busca
algum biombo para não enxergá"-la ou algum oleozinho para distraí"-la.
Nesse gesto, esse homem continua dirigindo seu olhar para a caixa e não
para a ferida. Outros há que tomam a direção contrária: ao invés de
legitimar a caixa pré"-existente, passam a desconfiar dela e autorizar a
ferida. Homens assim, capazes de um gesto de desrecalque, homens
machucados pela atrofia do existente, mas saudados pelo pressentimento
de outras possibilidades, encontram em Nietzsche um amigo e um
companheiro de travessia.

Há na obra de Nietzsche, uma parte negativa, em que ele examina como se
deu a montagem e com quais materiais foram construídas as caixas que
encontramos e que nos recebem nesse mundo, e há um momento afirmativo,
em que somos chamados a atravessar as medidas já instauradas e pisar no
inédito de outras medidas. Se a parte genealógico"-desconstrutiva da obra
retira a força do poder conformador das caixas, a parte afirmativa
convida o homem a tornar"-se um meteoro, o bloco errante que recebe
medida da própria trajetória, dos encontros e choques com outros corpos.
Receber medida da própria trajetória, e não de uma virtude prévia e
exterior, implica em desconhecer"-se, em não se saber a que veio. Implica
em não nomear antecipadamente a virtude. Se nas éticas tradicionais o
bem já está nomeado e já se encontra estabelecido, em Nietzsche a
virtude é um outro, um desconhecido que me habita: ``Mas esse pássaro
construiu em mim seu ninho; por isso o amo·e o aperto contra meu peito
--- agora encuba em mim seus ovos dourados'' (Assim falou Zaratustra).
Isso significa que eu não conheço e não disponho da virtude; é ela que
dispõe de mim e a tarefa ética consiste em desprender"-se do eu
autobiográfico, alienado no ideal, o eu"-virtual e pré"-narrado pelo
rebanho, para tornar"-se integralmente o espírito da virtude que me
atravessa. Deves tornar"-te o pássaro, a ave estranha que te habita e
trazê"-la ao mundo sem cálculo e sem negociação- esse é o imperativo da
ética da dádiva, o chamado para que o miolo da alcachofra se torne pele.
Mas como é que se interrompe a avalanche autobiográfica e fictícia do
eu? Como é que se dá o acesso a essa alteridade chamada si"-mesmo? E a
resposta é: pela emergência do corpo. Pelos acontecimentos que tocam e
fisgam um corpo exposto… Ali onde alguém é tocado e atravessado
para além de todo e qualquer funcionamento racional, ali onde um espinho
cortou a carne e onde uma questão insiste em forma"-de"-ferida, ali é o
lugar onde o ``eu'' deve mergulhar e deixar"-se desmanchar. Trata"-se de
uma mudança na relação com o traumático. Ao invés do ``eu'' exorcizar a
ferida com a ajuda da ``sociedade'' e dos dispositivos psiquiátricos e
psicológicos, é a ferida que desmancha o eu e engata o homem no
si"-mesmo. Amparado no elemento próprio e pronunciado pela ferida, o
homem que atravessa vê o mundo normalizado como uma amputação gigantesca
das possibilidades humanas e os saberes desse mundo como uma flatulência
verbal de escravos fazedores de escravos.

O que os funcionários da saúde mental, por exemplo, chamam de surto
psicótico é, na verdade, um chamado ético para sair da lonjura"-abstrata
e enganchar"-se nas dobras do corpo. O~surto é a sorte de começar a
despedir"-se da mentira autobiográfica e tentar redigir uma
heterobiografia haurida na experiência viva. Nietzsche não fala desse
modo sobre o convite do surto, mas conta, na mesmíssima direção, de como
a doença o salvou da ausência"-de"-si e o colocou na direção da tarefa e
do elemento próprios. A~doença, na medida em que ele não deu mais
ouvidos à palavra"-médica, noticiava que ele ainda não havia chegado a
pôr os pés no hospitaleiro. A~doença dizia: ``qui ainda não é teu lugar,
Friedrich! Esse país, esse alimento e esse ar, esse modo embolorado de
estudar e trabalhar, nada disso é bom e ressoante para ti, amigo! Tudo
aqui esmaga o teu si"-mesmo!!'': A doença é passaporte de migração, aviso
de que ainda não se está no elemento. Mas como é maravilhoso viver
tantos anos junto ao que não é propício, junto de elementos opressores!
Então, o si"-mesmo aguarda, feito uma fera hibernada, e tem de mostrar
toda sua força até encontrar o ressoante.

Isso mostra que não sabemos nada do si"-mesmo. Ele é um animal dobrado.
Um Finismundo, um Odradeck, um pássaro de nome difícil. Esse animal vai
se desdobrar ou não --- e revelar seu estilo, seu rugido --- no encontro
com lugares, livros, pessoas ou coisas. O~si"-mesmo, essa alteridade em
relação ao eu, está dobrado no corpo e depende do encontro com
alteridades (ares, obras, acontecimentos) para aparecer ou abortar. Daí
o caráter maravilhoso e misterioso de uma vida humana. Não há medida
prévia para ela! Vive"-se e é a partir daquilo que encontramos que o
si"-mesmo pode emergir e vir ao mundo ou ficar encruado e abortar. Nada e
ninguém sabe dizer de antemão o que, onde e como alguém vai ser tocado e
atravessado. Por isso, a autobiografia de Nietzsche é, na verdade, uma
heterobiografia\footnote{Remeto o leitor ao livro de Peter Sloterdijk sobre Nietzsche (O Quinto
``Evangelho'' de Nietzsche). Uso o termo heterobiografia porque
publiquei uma heterotanatografia (``Esse menino aí'') no livro Certeza
do Agora. Heterotanatografia não é o contar dos acontecimentos e
encontros ôntico"-positivos que desdobram o si"-mesmo. Heterotanatografia
é o narrar do desencontro radical daquele que não pôde nascer e assistiu
à produção e à montagem de si"-mesmo como simulacro. Heterotanatografia é
um dizer gnóstico"-kafkiano daquele que está paralisado no estranhamento.
Se o heterotanatógrafo, ``nascido'' na fenda, desce até um corpo
pressentido, então ele redige uma heterobiografia. No caso de ``Esse
menino aí''; convivem o estranhamento e o pressentimento, por isso, ele
contém elementos dos dois tipos de grafia.} , pois o canto de amor ao si"-mesmo é
um canto de louvor aos outros e narrar o simesmo é dizer o outro.

Escreve uma hetorobiografia aquele que, tendo saído para fora da redoma
do ideal e para fora da detenção identitária, se pôs à deriva e,
amparado no desconhecimento, experimentou cada coisa com a autoridade do
próprio corpo! Esse então pode escrever: Sim, aqu\{ eu estive!! Ali não,
ali fui reduzido à sombra e à marionete! Escreve uma heterobiografia
aquele que, ao olhar dentro de si, viu ``a sociologia e o
vazio''\footnote{Nietzsche não diria o ``vazio'', mas faço uso, mesmo assim, dessa frase
de Gottfried Benn.}  e, em meio à multidão dos nomes da história,
reconhece aqueles em que algo pipocou e diz: Sim, na faísca do nome
dessa divindade oriental desdobrou"-se o meu mais íntimo. Escreve uma
heterobiografia aquele que cancelou o ``eu'' autobiográfico e tornou"-se
um outro, um outro que é mais presença do que o eu. Esse realizou o
conteúdo da famosa frase de Rimbaud --- {]}e est un autre --- e, se
narrou a peripécia dessa metamorfose e dessa transgressão, escreveu
assim uma heterobiografia. Escreveu uma heterobiografia aquele que
desistiu de se livrar da ferida, mas tomou palavra naquilo que a dor da
ferida sabia. Todos esses são migrantes da ponte, todos esses podem
morrer em festa e deixar um legado e um convite.

É certo que 120 anos atrás --- tempo em que Nietzsche redigiu tanto o
convite a uma ética da singularização (Assim falou Zaratustra) quanto a
efetivação do sorriso da singularização (Ecce Homo --- Como Alguém se
Torna o que é) --- era mais fácil conduzir o homem a si mesmo. Hoje em
dia, no início do século \versal{XXI}, a situação é mais complexa por causa do
avanço do domínio médico"-científico e suas instâncias de nomeação e
neutralização da dor. Se Nietzsche pôde tomar a si mesmo em mãos,
fechando os ouvidos ao dizer médico e pôde encontrar espaço"-tempo para o
adoecer e o convalescer, hoje, sequer conhecemos a palavra
``convalescençà: Já não há tempo para as lentas maturações, o barulho
informacional atinge qu lquer lugar e o dizer científico engoliu a
própria compreensão da dor. Esse dizer se tornou mais e mais hegemônico
e totalitário. Com o progressivo definhar da palavra literária ---
convertida em abstração estética e confete de signos --- e com a
extinção do filosofar --- convertido em assunto de profissionais
competentes, autoridades do \versal{QI} e dos seis idiomas --- , o homem perde a
possibilidade de encontrar um repertório para enunciar a dor. E~a dor é
ela mesma uma delação do deserto! Mas os saberes barram precisamente o
acesso a essa notícia. Impedem de enxergar para onde a dor
aponta… Hoje, o homem que sofre precisa da sorte muito rara de
encontrar um espaço desmobilizado (espaço da amizade) para autorizar a
dor e desvendar o que ela sinaliza\footnote{Resta saber se os consultórios de psicanálise podem ou não ser
considerados espaços desmobilizados. De um lado, a psicanálise é o único
espaço de escuta da dor em nossa sociedade e ela se apresenta como uma
ética da singularidade. De outro lado, ela faz a linguagem mobilizada da
metafísica moderna (sujeito, inconsciente, representação) invadir a
província natal. Marcada pelo desejo de teorização e cientificidade,
trabalha com quadros psicopatológicos, o que atorna uma máquina de
destruir epifanias e patologizar fendas.} . Fora dessas
condições, ele é necessariamente devolvido ao regime autobiográfico com
uma ou duas costuras na ferida e mais um ou dois bálsamos neuroquímicos
para parar de doer. Assim remendado; segue a vida sem se apropriar de si
mesmo, dublando"-se em termos de competência e trabalho, isto é, conforme
a identidade pré"-definida pela moldura epocal e pela gaiola da vez, e
morre a morte dos supérfluos, dos que desconhecem a
narrativa"-efetiva"-da"-carne, o sorriso sangüíneo da singularização.

Nietzsche escreveu: ``Ouçam"-me, pois eu sou tal e tal. Sobretudo não me
confundam!'' (Ecce Homo ). Aqui, quem escreve molha o tinteiro no
sangue. Aqui não se faz literatura! Cada sílaba está haurida no corpo e
na experiência. Se o Nietzsche"-cupim, o Nietzsche"-corpo"-explosivo, o
Nietzsche"-genealógico"-descontrutivo conduz à imagem do pensador da
vontade de potência e do filósofo pan"-interpretacionista --- afinal, o
trabalho da desconstrução desnaturaliza, desessencializa e
desuniversaliza as construções culturais e as avaliações morais,
mostrando que tudo o que veio a ser e que teve uma gênese histórica pode
ser desfeito e refeito de outro modo --- , o Nietzsche heterobiógrafo
afirmativo conduz a uma outra imagem, a do sóbrio e simples pensador do
atravessamento. Simples porque o homem do corpo"-exposto apenas acolhe
aquilo que se impõe. Ele não tem o poder de produzir e nem de
desconstruir. Acolhe o indesconstrutível! Assim é o caso do próprio
filósofo Friedrich Nietzsche, cuja obra é dádiva de um corpo indomável!
É um corpo instável --- ora doente, ora saudável, ora forte, ora fraco,
esquivo e oblíquo, deslizando feito uma enguia diante das lupas
medicinais --- que possibilitou o olhar migrante do filósofo da
transvaloração.

A alternância radical entre saúde e doença deu a Nietzsche a
possibilidade de transitar por terras variadas e verificar onde e como
floresciam este ou aquele tipo de moralidade: ``Da ótica do doente ver
conceitos e valores mais sãos e, inversamente, da plenitude e certeza da
vida rica descer os olhos ao secreto lavor do instinto de décadence''
(Ecce Homo). Essa experiência, enquanto experiência crucial, precede
Nietzsche… e dir"-se"-ia aqui que um menino de corpo indomável
comanda um filósofo a deslocar perspectivas. Nietzsche reconhecia tanto
a existência dessa ``experiência"-precedente'' que no livro Genealogia da
Moral, por exemplo, ele conta que, ainda menino, aos 13 anos de idade,
escreveu uma redação na qual Deus aparecia como o pai do mal e se
pergunta se não era isso o que o seu ``a priori'' exigia dele. Isso
significa que a questão acerca da origem do bem e do mal havia brotado
tão cedo em Nietzsche que ele a considerava um ``a priori'': Ora, tanto
nesse exemplo, como no do corpo indomável, estamos diante de
atravessamentos. Marcas ante as quais o filósofo se rende. Aqui, já não
vemos o martelo demolidor de ídolos, mas o respeito ao que é, o
curvar"-se ante o alfabeto misterioso de que fala Henri Michaux:. Nesse
sentido, a obra do filósofo andarilho é um convite de amigamenta do real
em nós, um devir"-vertical até a assunção do material eclodido na
província natal. É~também um nutriente para que um homem possa manter"-se
na idiotia mais pura e guardar o seu dizer afastado das instituições e
da dizibilidade adestrada.

Há, portanto, além do Nietzsche mais famoso, que afirma que tudo é
interpretação, e que a palavra ser é um vapor, um outro mais dócil, que
diz que uma rosa é uma rosa é uma rosa… Se, de acordo com o
Nietzsche mobilizado, não há paragem para a interpretose infinita e nós
próprios não passamos de caixinhas de metáforas que se criam e se
descriam, para o Nietzsche de Ecce Homo, um homem é o que é e se deixa
dizer em uma ou duas experiências narráveis em dois aforismos de 15
linhas. Há que se pensar simultaneamente as duas coisas: de um lado a
dispersão moderna, a desconfiança radical e a abertura do demiurgo capaz
de alterar a rosa e o ser humano, e, de outro, a redução pará o simples,
o corpo e a proximidade, o atravessamento e a lucidez terrestre. Há que
se pensar os dois pólos simultaneamente, pois Nietzsche é um pensador da
fronteira na qual ainda nos encontramos hoje. Nele convivem as duas
direções, ele era habitado por ambas e levou as duas ao limite. Assim,
se o ``ontólogo'' da vontade de potência e do eterno retorno pode
realmente ser lido como o pensador do regime da presença (do modo de ser
dos entes) na hora do domínio planetário da técnica, ele é,
simultaneamente, o pensador daquilo que ninguém produz e que não se
cria. O~Nietzsche pensador do atravessamento suspende a fobia da verdade
e a apatridade do valor para recolher o acontecimento de uma vida. Essa
desmobilização é ela própria uma realização e uma superação da filosofia
e, quando isso acontece"-o recolhimento puro de uma vida--- , uma palavra
é liberada para poder falar. Desaba a polícia teórica da verdade (o
regime de qualquer universal), pois um homem tornou"-se verdadeiro. Seu
idioma tornou"-se carne e ele dá o testemunho de uma passagem. Então esse
homem reafirma a existência de uma vida verdadeira, ratificando o
sorriso indomável num rosto de júbilo.

\versal{REFER}Ê\versal{NCIAS}

\emph{De Nietzsche}

\begin{itemize}
\item
  \versal{NIETZSCHE}, Friedrich. Así Hablo Zaratustra. Madri, Alianza Editorial,
  1999.
\item
  \_\_\_\_\_\_\_\_. Ecce Homo. Como Alguém se Torna o que é. São Paulo,
  Companhia das Letras, 2007.
\item
  \_\_\_\_\_\_\_\_. Crepúsculo dos Ídolos ou Como se Filosofa com o
  Martelo. São Paulo, Companhia das Letras, 2006.
\item
  \_\_\_\_\_\_\_\_. O~Anticristo e Ditirambos de Dionísio. São Paulo,
  Companhia das Letras, 2007.
\item
  \_\_\_\_\_\_\_\_. Genealogia da Moral. Uma Polêmica. São Paulo,
  Companhia das Letras, 1998.
\item
  \_\_\_\_\_\_\_\_. Nietzsche. São Paulo, Abril Cultural, 1978. Coleção
  Os Pensadores.
\end{itemize}
\emph{Sobre Nietzsche}

\begin{itemize}
\item
  \versal{ANDRADE}, Daniel P\,Nietzsche- A Experiência de Si como Transgressão
  (Loucura e Normalidade). São Paulo, Annablume.
\item
  \versal{BATAILLE}, Georges. Sobre Nietzsche --- Voluntade de Suerte. Madri,
  Taurus, 1986.
\item
  \versal{DELEUZE}, Gilles. Nietzsche. Lisboa, Edições 70, 2001.
\item
  \_\_\_\_\_\_\_\_. Nietzsche e a Filosofia. Porto, Rés"-Editora, 2001.
\item
  \versal{FORNAZARI}, Sandro. Sobre o Suposto Autor da Autobiografia de
  Nietzsche. Reflexões sobre Ecce Homo. São Paulo, Discurso Editoral,
  2004.
\item
  \versal{GIACÓIA} Jú\versal{NIOR}, Oswaldo. ``Algumas Notas sobre `A Grande
  Políticà'\,''. \emph{in}  \versal{AZEREDO}, Vânia (org.). Falando de Nietzsche. Ijuí,
  Editora Unijuí, 2005.
\item
  \versal{GUERY}, François. Ainsi paria Zarathoustra. Volonté, Vérite, Puissance
  (9 chapitres du livre li). Paris, Ellipses, 1999.
\item
  \versal{HALÉVY}, Daniel. Nietzsche, uma Biografia. Rio de Janeiro, Campus,
  1989.
\item
  \versal{HÉBER}-\versal{SUFFRIN}, Pierre. O ``Zaratustra'' de Nietzsche. Rio de Janeiro,
  Jorge Zahar Editores, 1999.
\item
  \versal{HEIDEGGER}, Martin. ``Quem é o Zaratustra de Nietzsche?'': \emph{in}  Ensaios
  e Conferências. Petrópolis, Vozes, 2002.
\item
  \_\_\_\_\_\_\_\_. Nietzsche. Vol. \versal{II}. Rio de Janeiro, Forense
  Universitária, 2007.
\item
  \versal{MACHADO}, Roberto. Zaratustra. Tragédia Nietzscheana. Rio de Janeiro,
  Jorge Zahar Editor, 2001.
\item
  \versal{MARTON}, Scarlett (org.). Nietzsche Pensador Mediterrâneo. A~Recepção
  Italiana. São Paulo/Ijuí, Discurso Editorial/Editora Ijuí, 2007.
\item
  \versal{NUNES}, Benedito. O~Nietzsche de Heidegger. Rio de Janeiro, Pazulin,
  2000.
\item
  \versal{SLOTERDIJK}, Peter. O~Quinto ``Evangelho'' de Nietzsche. Rio de
  Janeiro, Tempo Brasileiro, 2004.
\end{itemize}
\emph{Outras}

\begin{itemize}
\item
  \versal{ARENDT}, Hannah. O~Que é Política? Rio de Janeiro, Bertrand Brasil,
  2006. \versal{HAAR}, Michel. Heidegger e a Essência do Homem. Lisboa, Instituto
  Piaget, 1990.
\item
  \versal{HEIDEGGER}, Martin. Ser e Verdade. Petrópolis, Vozes, 2007.
\item
  \_\_\_\_\_\_\_\_. ``Sobre o `humanismo'\,''. \emph{in}  Heidegger. São Paulo,
  Abril Cultural, 1979. Coleção Os Pensadores.
\item
  \versal{HESSE}, Hermann. O~Jogo das Contas de Vidro. Rio de Janeiro, Record,
  s/d.
\item
  \versal{MANN}, Thomas. Doutor Fausto. Rio de Janeiro, Nova Fronteira, 1984.
\item
  \versal{MICHAUX}, Henri. Épreuves, Exorcismes 1940--1944. Paris, Gallimard,
  1973.
\item
  \versal{MUSIL}, Robert. O~Homem sem Qualidades. 3 V\,Lisboa, Livros do Brasil,
  s/d.
\item
  \versal{RIMBAUD}, Arthur. Uma Estadia no Inferno. Rio de Janeiro, Civilização
  Brasileira, 1977.
\item
  \_\_\_\_\_\_\_\_. O~Rapaz Raro. Iluminações e Poemas. Lisboa, Relógio
  D'Água, 1998.
\item
  \versal{SLOTERDIJK}, Peter. A~Mobilização Infinita. Lisboa, Relógio D'Água,
  2002.
\end{itemize}
