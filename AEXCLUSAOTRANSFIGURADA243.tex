\chapterspecial{A Exclusão Transfigurada}{}{}
 \epigraph{Esta é vossa sede, o chegar vós mesmos a ser oferendas e presentes.
\versal{NIETZSCHE}
\emph{Da virtude que presenteia}}{\versal{NIETZSCHED}a virtude que presenteia} 

\begin{flushright}\emph{Para Adriana Canepa,}\end{flushright}
 \begin{flushright}\emph{ioiô feito de pérola que atingiu a minha vida.}\end{flushright}


Entre outras coisas eu, Kafka,…

Durante muito tempo eu pretendi nascer para dentro do mundo. Me parece
bastante estranho que eu tenha nascido para fora do mundo. Como é
possível que, ao nascer, não se nasça para o interior do mundo, mas para
uma zona exterior e ilegal? Minha condição é de aposentado prematuro;
desde o início desencarregado do serviço do mundo por déficit de algum
liame essencial. Em mim o liame era de gelo, amarra logo derretida e
desfeita.

Mas eu sempre desejei regressar ao engano dessa aderência. Eu via todo
mundo vivendo pelo lado de dentro. Eu também queria viver na ilusão de
estar vivendo por dentro. Os homens, quando vivem pelo lado de dentro,
quando se encontram na ativa, eles estão tão bem encostados no ente e
integrados ao mundo, que são inteiramente identificáveis. Ontem mesmo,
lá no bar, com o Werfel, o Max e os outros, eu os via tanto que me
assustava!! Eu os percorri um a um e fui soletrando a identidade deles
até que chegou a minha vez e eu percebi que seria impossível dar de mim
mesmo alguma localização. Levantei"-me e fui até o banheiro e pude,
então, perceber que o meu umbigo era um ponto de interrogação. E~mais,
ao olhar melhor e mais detidamente para o meu umbigo, notei que nele se
abria um buraco sem fim. Cheguei a achar engraçado, pois embora eu odeie
metáforas, comparei"-me a um queijo suíço cheio de buraco, mas um queijo
suíço já quase inteiramente comido e devorado por tantos ratos que só
sobrava mesmo um buraco"-em"-si. Mas é isso. Eu volto para a mesa e eles
parecem todos bem enquanto eu fico o tempo todo roído e atravessado por
esse desassossego terrível, por essa pergunta imensa… O Max vive
sempre me elogiando, dizendo que eu sou isso e sou aquilo, mas ele não
sabe que eu não passo de um hieroglifo sem sentido e que minha vida é
apenas uma marcha congelada, pois eu, ao contrário dele e de suas
peripécias e experiências múltiplas, tive apenas a experiência de não
ser contemporâneo de nenhuma experiência.

Experiências e acontecimentos se dão do lado de lá da fronteira, do lado
dos que nasceram para dentro do mundo. Como eu nasci para fora, eu não
consigo alcançar o reino das experiências. Numa carta que escrevi para
Milena no ano de 1922, eu disse que ``minha vida é a hesitação ante o
nascimentd: Nascer, para além do sentido biológico, é acolher
determinações, é ganhar uma identidade no mundo. Ora, como eu zanzo na
zona pré"-natal, eu permaneço indeterminado e desreferencializado. O~mundo dos nascidos (dos subjetivados) é para mim uma terra estranha onde
entro como uma espécie de etnólogo invejoso. Tudo que para eles é um
habitual partilhado, para mim é algo estranho a ser decifrado… e
aliás, grande parte da interpretose infinita que aparece nos meus textos
deriva daí. Deriva de que devo me esforçar na direção do básico. Existe
miséria maior do que ter de se esforçar na direção do básico?? Além
disso, minha posição de excluído do mundo e de detido na antecedência do
nascimento (no meu diário escrevi ``ainda não ter nascido e ser obrigado
a passear pelas ruas e a conversar com as pessoas'') é dolorosa, porque
há em mim uma imensa saudade da vida não vivida, uma nostalgia da vida
desperdiçada. Ah! que promessa, a vida humana, como eu rondei essa
promessa. Como eu a pressenti atrás do véu da exclusão! Muitas vezes
achei que um belo dia eu iria acordar e pronto!! eu estaria dentro da
vida, partilhando e participando com os outros. Então eu simplesmente ia
estar na mesa das pessoas animadas e eu estaria animado também e não
como naquela pensão, naquela noite em que anotei no meu diário:
``infinitamente assombrado diante de uma reunião animada de pessoas'':
Sempre assombrado, com o rumor das partidas, com os planos de férias
daqueles que viajavam; um dia ainda --- quanto messianismo! --- eu iria
sentir aquilo tudo por dentro.

Eu gostaria de sentir afetos diretos e não apenas afetos
transcendentais. Explico melhor: eu gostaria de dizer eu gosto disso, eu
sinto isso e não estar espantado ou assustado por sentir isso ou aquilo.
Quem vive na antecâmara do destino tem afetos transcendentais, não olha
a caneta e segue em frente… mas fica assambrado que a caneta
seja… daí também que nos meus textos haja a descrição
hiperminuciosa de rostos e pessoas. Quem reside no senso comum não
precisa e nem faria minhas descrições. Minhas descrições são de alguém
que não pertence e nem mora na representação partilhada, mas no assombro
da aparição. Se descrevo uma mulher não é porque estou eroticamente
encantado com sua forma, mas porque estou ontologicamente perplexo de
que ela exista e não antes o nada (segundo o repertório de um filósofo
alemão que ficou conhecido logo depois que eu morri). Posso encurtar
essa explicação dizendo que estar na antecedência do nascimento torna
teu olhar um portal do deslumbramento. Outro dia caminhando na calçada
vi um casal passeando com dois cães, um grande do tipo Schnauzer e outro
pequenino da raça Poodle. Subitamente senti que aqueles animais andavam,
se moviam: eram dois casais, um de bípedes e um de quadrúpedes. Iam
esvoaçantes na calçada e eu fiquei tão atingido e maravilhado que não
parava mais de olhá"-los. Meu olhar era tão intenso que a mulher parou
indignada e, logo em seguida, seu marido também passou a me olhar. Eles
estavam meio desconcertados pelo modo como eu os via; talvez eu os tenha
visto demais…

Minha vida é um casulo transparente e inacessível. A~ferida que não
consta dos mapas dermatológicos. A~sinapse escondida que não consta da
psiquiatria. Sei que nenhuma metafísica me apanhará. Ela é alérgica ao
buraco e à ausência. No fundo, ninguém quer saber da notícia que divulgo
e que carrego. Num mundo pavimentado não se quer escutar ouvir falar de
buraco e de rachaduras! Mas eu divulguei a notícia com tamanho ardor que
eles acabaram por incluir"-me em sua comunidade. Paraisso inventaram até
uma nova profissão: ``anunciador de buracos'' e escritor. Eu fiquei
muito feliz até que notei que o meu rosto estava ficando pavimentado e
eu tive de fugir da comunidade que me acolhera. Voltei para dentro do
buraco e me calei.

Ser um habitante do buraco é deslizar perpetuamente no tempo da
inconsistência do exílio. A~sombra da morte, a sombra da ausência é
onipresente. Se penso no Max, por exemplo, ele se move na consistência e
no tempo do amparo no mundo. Para alguém como ele, alguém que tem lugar,
a morte, o impossível e a dor aparecem sempre mediados pelo mundo; são
ameaças e medos incrustados que pipocam cá ou lá em objetos ou situações
(uma espécie de sombra no objeto): ir a um médico, ficar doente e
descobrir uma pinta debaixo dos pés etc. são pequenas frestas e
rachaduras para o horror, mas a base é o estar"-em"-casa"-no"-mundo. Ao
contrário, o homem do desamparo, aquele que se encontra, como eu,
imantado pelo impossível, este é olhado direta e constantemente pela
morte. Não há trégua nem esquecimento. Está engolido no espaço da
rachadura. A~casa é um além onde não se chega. Fernando Pessoa --- um
escritor português que morreu n anos depois de mim, ele morreu em 35 com
46 anos de idade e eu em 24 com 41 anos, ele de cirrose hepática e eu de
tuberculose; mas ambos estávamos bastante em casa na hora de morrer ---
escreveu no Livro do Desassossego que a vida é o estar numa estalagem
esperando a diligência do abismo. Então, se é m.esmo assim, enquanto os
homens como Max, os homens"-do"-mundo, entram para o hotel e desfazem as
malas --- e vão até o salão conversar, tendo inclusive fechado as
janelas para não escutar o apito do trem da morte ---, o homem do
desamparo nem chega a cruzar a porta do hotel. Fica em pé, na estação,
sem desfazer as malas. Sabe que o trem já está apitando.

Assim eu me vejo: detido na estação, olhando para dentro do hotel e
impedido de entrar, pois eu já pertenço ao apito do abismo. Eu já não
posso esquecê"-lo… eu gostaria de entrar no hotel como o K do
Castelo. Mas o K sabe que não vai entrar. Ele já se sabe um homem
separado; sabe que não cruzará a porta de entrada\footnote{Faço minhas aqui as palavras de Maurice Blanchot sobre os meus dois
romances póstumos. Aliás, todo esse parágrafo é tributário do que
Blanchot escreveu sobre os meus livros. Não é a primeira vez que tomo
emprestadas as palavras do meu leitor mais profundo, daquele com quem
mais aprendi sobre mim mesmo. Blanchot compreendeu minha experiência
para além do que eu mesmo pude compreender em vida e eu nunca imaginei
que alguém pudesse desenvolver todo um pensamento sobre a essência do
literário e uma topolo gia da situação humana a partir da experiência
que me atravessou. No dia 3 de junho de 1924, dia em que morri num
sanatório de Kierling, Blanchot tinha 17 anos e estava em outro país. Se
não pudemos nos saudar em vida, agora posso tornar"-me ele e colher suas
palavras; afinal, damos testemunho da passagem por um mesmo lugar e por
uma mesma questão.} . Já
Josef K, meu personagem do Processo, pensa gozar de boa situação dentro
do hotel. Pensa mesmo pertencer ao hotel: abre seu jornal diante da
lareira e tira até uma sonequinha. Ele é um alto funcionário de banco e
tem uma situação excelente. Josef K não percebe, não se dá conta de que
seu nome já foi apagado da lista dos hóspedes, não percebe que os
funcionários já não se dirigem mais a ele do mesmo modo e que ele já não
dispõe mais da chave do quarto que pensa estar no seu bolso. Josef K não
nota, não se dá conta de que ele também foi recusado e banido do
interior do hotel. Gruda seus olhos no jornal, mas as letrinhas começam
a despencar no seu colo e as camareiras estão gracejando. Abre"-se,
então, um rombo, um furo na página do jornal, e Joseph K ainda não
percebe que a segurança do Dentro na verdade já se esfacelou e o
processo é justamente a tomada de corisciência dessa exclusão radical,
desse ostracismo e dessa morte, com a qual, desde a origem, K foi
carimbado… Por isso, chamei K de K no Castelo enquanto Josef K
pensa ainda dispor de um nome, pois acredita estar bem ancorado no mundo
até que descobre, progressiva e paulatinamente, --- através do Processo
--- que tudo já lhe havia sido arrancado… Para tornar ainda mais
fácil tudo isso, basta você imaginar que há no hotel um empresário
poderoso. Ele tem filhos maravilhosos, uma linda mulher e uma polpuda
conta bancária. Sente"-se inteiramente protegido. Mas, quando ele se
senta na espreguiçadeira e levanta os braços, você nota que há uma
mancha imensa, uma mancha violácea e negra debaixo de seus braços. Ele
ainda não sabe, mas você já sentiu que todo aquele pequeno mundo
enquanto pseudofortaleza já está escorrendo feito areia entre os dedos.
Nenhuma pirâmide domará a morte. Provavelmente quando o empresário
perceber a mancha, ele será tomado por um calafrio. Será tocado pela
notícia da mortalidade e, dentro daquele hotel quente e acolchoado,
sentirá imediatamente o frio de fora e o frio de lá. Sentir aqui o frio
de lá é outra sentença bastante lúcida desse português, o Fernando
Pessoa. Há muitas coisas parecidas entre nós, embora ele seja mais um
aristocrata do negativo e descreva um K orgulhoso e desprezador do
nascimento, enquanto eu jamais desprezei a nascença; apenas mantive ante
ela uma hesitação e uma ambiguidade… Para mim, sempre foi muito
difícil e doloroso ser filho do impossível e estar excluído dos caminhos
habituais ou da queda"-aderência no possível, como diria o filósofo
alemão. Se Fernando Pessoa viu no impossível uma espécie de atestado de
nobreza que o distinguia do frenesi do gênero humano como macacada
metafísica, eu, ao contrário, sempre senti a filiação pelo impossível
como uma espécie de maldição e condenação. Sempre temi a solidão
completa e achei que ia morrer sem ter vivido. Numa carta, já nem me
lembro para quem, pois eu escrevia uma quantidade enorme de cartas, eu
disse que: ``eu mesmo não posso seguir vivendo, uma vez que não vivi,
fiquei no barro, e a fagulha que não soube transformar em fogo servirá
apenas para iluminar o meu cadáver..:''. Eu jamais parei de me
perguntar: o que se passou que me tornei um ouriço para todos os lares?
O que se passou, que só olhei as casas pelo lado de fora? O que se
passou, que desde o início meu corpo foi apenas a tumba e a pena de
morte, mas não o lugar do festejo e do encontro? Por que andei
petrificado contemplando o mundo, mas não pude possuí"-lo na matéria
quente? Dobrei"-me no frio do transcendental e não me esparramei na vida.

É preciso dizer também que algumas vezes eu até esquecia o impossível. É~como se, num passe de mágica, eu fosse normal, eu fosse um sujeito
nascido para o lado certo (aquele para quem o impossível está
recalcado). Mas subitamente tudo caía. e eu esquecia do impossível, o
impossível não se esquecia de mim, e vinha me puxar! Derrubada geral! Eu
era jogado feito um ioiô de uma posição K para uma Josef K e de uma
posição Josef K para uma K, mas a partir dos 30 anos a leveza foi
desaparecendo mais e mais, e minha fidelidade ao buraco foi se
intensificando. Quantas vezes abocanhado pelo impossível não sonhei em
ter um bilhetinho para desparafusar"-me e nascer do lado correto, sendo
ejetado para dentro e não para fora como da primeira vez! Quantas vezes
caminhei à noite pela cidade de Praga olhando para as luzes no interior
das casas e dos prédios e me perguntando: mas o que é que estão fazendo?
Como é possível esta vida? Como é possível alocar"-me dentro da presença,
alcançar essa dimensão e aí permanecer? Como é possível ter alguma
biografia? Seria possível introduzir esse buraco (o desastre, o
impossível) na história e no tempo sem destruí"-los? Como é que eles não
estão engolidos, por meio de que mágica migram para a segurança do mundo
e de um si"-mesmo? O que é, afinal, que eu sei e que não deveria saber?

Questões como essas eu as ruminei a vida inteira. Meu corpo e minha dor
sempre foram a cobaia dos meus pensamentos e os meus pensamentos sempre
foram a cobaia de meu corpo e da minha dor. Esse é o círculo do
escritor. Nos meus diários eu escrevi que ainda que eu durasse
eternamente, a aproximação a Canaã permaneceria infinita… Isso
significa que o buraco jamais será tapado. Não haverá nunca o grande
domingo ontológico, a festa dos gordinhos abraçando o absoluto na
presença. Nesse ponto eu descobri, como Nietzsche, a dor e o devir, e o
devir mesmo como dor. Qualquer absoluto, qualquer sossego é apenas
alguma construção dos amigos da ilusão. Por mais que a biotecnologia
futura nos dê alguma imortalidade biológica, não se domará a marcha
explosiva do sol, não se domará o colapso astrofísico nem a chuva de
asteroides. A~aproximação a Canaã sempre permanecerá infinita e demorará
infinitamente.

Nietzsche percebeu exatamente o que vi. Mas eu nunca compreendi por que
ele decidiu eternizar o devir falando num eterno retorno do mesmo. Para
dizer a verdade, sempre achei essa noção um tanto grosseira, pois ela
impede que experimentemos o abismo da mortalidade. Outra coisa
interessante é que, embora Nietzsche também não tenha encontrado lugar
nem hospitalidade no mundo, ele não cessou de tentar atravessá"-lo, não
cessou de rasgar e desconstruir o mundo… não cessou de retirar
força do instituído a fim de que ele, Nietzsche, pudesse nascer e seu
corpo pudesse surgir nalgum lugar, enquanto eu apenas nomeei esse mundo
que encontrei e, embora ele me fosse inteiramente inóspito, eu quis,
ainda assim, pertencer a ele. É~muito difícil compreender isso, e eu
mesmo só vejo isso agora, 8o anos depois da minha morte, e com a ajuda
de outros interlocutores… Nietzsche sentiu o estranhamento e a
apatridade, mas ele sentiu, simultaneamente, um pressentimento e um
apelo do além"-homem. Ele encontrou a terra já em vias de dominação
completa e o homem vestido com a máscara do animal racional, mas ele
perguntou de onde tirava legitimidade essa equação segundo a qual ser
homem é igual a ser animal racional. Eu também encontrei a mesma
situação, encontrei a máscara fria do animal racional e estranhei"-a até
o fundo de meus ossos, mas mesmo assim eu senti que tinha de vestir essa
máscara para tornar"-me um cidadão do mundo. Eu não senti o
pressentimento; eu fui fiel ao meu estranhamento, enquanto Nietzsche
tentou atravessar o estranhamento. Ele teve a ousadia de perguntar: e se
a máscara da representação, a máscara do animal racional destrói e
machuca o rosto vivo do homem? Ele perguntou o que há para além da
máscara do animal racional que não seja apenas a exclusão do mundo a ela
pertencente. Eu enti que, se não vestisse a máscara do animal racional e
a máscara do trabalhador competente, eu seria apenas nada. Para mim só
havia esse mundo hiperdeterminado, assustador e incompreensível, e o
nada, o ser ejetado para o pavilhão dos excluídos. Não havia, para mim,
outras possibilidades. Ou a inserção no mundo ou a exclusão. Eu não pude
suspeitar que a exclusão --- ela mesma --- poderia assinalar algo
positivo, ser o passaporte para alguma migração desconhecida…
minha situação era totalmente claustrofóbica: eu encarava o minotauro
gelado, o teatro fantasma do animal racional na mesma medida em que ele
me encarava e me atravessava. Eu olhava o horror e o horror olhava
dentro de mim: paralisado fui anotando tudo o que via e, talvez, por
isso, pude descrevê"-lo e mostrá"-lo como ninguém. Não tentei atravessá"-lo
como Nietzsche, nem pude analisá"-lo'' e dissecá"-lo como Simmel ou Marx.
Eu apenas o recolhi e o disse; eu o soletrei com o olhar do poeta sem
pálpebras. ``Vou registrar esse enigma que me devora'' --- talvez essa
tenha sido minha máxima.

Hoje sei que fui o dizente de um mundo inóspito. Sei que --- como
escreveu Ricardo Piglia, um escritor argentino que admira muito os meus
Diários --- fui o Homero do século 20, fui o aedo do mundo sem poema, um
Homero sem Ítaca e sem regresso. Sei também que o mundo para o qual eu
tanto quis entrar é precisamente o mundo cuja violência excluiu e não
deu lugar ao poeta que me habitava. Sei que mesmo o negativo e o buraco
são refúgios; são mais suportáveis de que o ofuscamento de uma realidade
inteiramente pronta e despregada/descolada de qualquer brotar e de
qualquer florescer. O~poeta só tem lugar na guarda das nascentes. Na
desmedida da amizade. Quando ele encontra um mundo e homens inteiramente
medidos e fixados na positividade (mundo metafísico dos
trabalhadores"-funcionários), esse encontro só pode ser alérgico e
doloroso. Eu estranhei esse mundo e na escrita recolhi esse
estranhamento. O~estranhamento guarda a memória do humano e para além
dele mesmo (estranhamento) pressente um mundo onde um rosto possa pulsar
e vibrar no encontro de um outro rosto vivo e não de figuras encaixadas
na função e na competência"-mundo.

Por isso eu escrevi numa carta para Max Brod que ``me parece que a
essência da arte não se explica mais que por tais considerações
estratégicas: para fazer possível uma palavra verdadeira dirigida de
homem a homem'': Eu só encontrei máscaras em estado de alerta e senti"-me
em dívida com o mundo mascarado. Eu, que precisamente estava impedido de
vestir qualquer máscara e qualquer uniforme, pois o poeta e o olhar que
é portal do deslumbramento ancoram"-se apenas na nudez; eu senti minha
nudez como uin erro, pois comparei"-a com o homem protegido por um
vestuário. Por causa dessa comparação oriunda da força do instituído,
senti"-me sempre culpado, sempre menos, eternamente deficitário…
Eu não pude perceber que minha nudez, minha fragilidade, eram a força do
olhar. Não pude ver que minha impotência metafísica, a incapacidade de
ajustar"-me ao mundo das medidas era, na verdade, a potência poética
pedindo passagem para inaugurar um outro mundo e uma outra vida. Não
percebi que o pensador do inóspito que fui abria o flanco para pensarmos
a hospitalidade e uma condição hospitaleira sobre a terra. Hoje,
passados 8o anos de minha morte, o positivo que nos foi concedido --- eu
escrevi que o ``positivo já nos foi concedido, só nos resta realizar o
negativo'' --- já atingiu a máxima saturação, e o estranhamento deu
lugar ao pavor e ao horror. Hoje qualquer criança que aporte nessa terra
extinta encontra bem menos espaço para o brotar e o florescer. Encontra
de imediato a câmara obstétrica do pré"-natal, encontra, em seguida, o
peito de silicone e o bottox, encontra o zumbi erótico da terra
devastada e não mais alguma mulher plantada no húmus e no silêncio de
seu próprio útero. Num encontro desses, para não ficar ofuscado e
queimado pelo mundo, recua"-se até a fenda, até o negativo, e aí, ou você
permanece lá e se torna um escritor ou um pensador --- caso consiga não
enlouquecer, caso suporte ser esbofeteado por todos os lados e permaneça
com o rosto todo machucado, mas permaneça sem se refugiar nos
constructos da loucura --- ou você é obrigado a cindir"-se e enviar ao
mundo os falsários, as coreografias fatigadas para transitar entre as
figuras do mundo.

Hoje, passados 8o anos de minha morte, até eu mesmo, que era quase uma
sombra, até eu mesmo que, ao visitar o meu primo, notei que ele acordou
por minha causa, e eu, então, disse a ele: ``Não se incomode; me tome
por uma aparição do seu sonho: até eu mesmo, que não queria incomodar
ninguém, tornei"-me uma grife iluminada, uma mercadoria distribuída até
em farmácias. Hoje encontramos muito mais luz, produção, competência e
informação. Quem aí não se encaixa, pois se trata mesmo de um encaixe,
quem não encontra morada na violência do produzir"-consumir, é
inteiramente posto de lado feito uma barata"-Gregor. E~o mais terrível é
que o apelo do mundo é tão intenso que toda dor sofrida é sofrida
passivamente e aquele que sofre e para quem tudo está estranho pensa que
o problema está nele e é com ele mesmo. Vai então para o psicólogo ou
para o psiquiatra ao invés de ir ao meu encontro e tornar"-se meu amigo.
Eu, passados 8o anos de minha morte, ainda espero por amigos reais que
preparem a mesa da hospitalidade e da amizade! Meu testemunho, tudo o
que escrevi, é uma garrafinha de alto"-mar lançada para aqueles que
sentiram o estranhamento ou que ainda não sentiram e estão para sentir
ou para aqueles que pensam estar em casa e que se encontram
identificados com o mundo, precisando então acordar e despertar.

Sempre aparecem alguns abrindo a garrafinha que lancei. \versal{JP} é um deles.
No ano de 2007, ele dedicou a mim dois aforismos. O~primeiro é
intitulado: ``Poeta em mundo sem poema'':

``Ontem eu soube de um menino que quase morreu durante uma punção num
hospital de São Paulo. Dois especialistas olhavam para as costas
enquanto a mãe lhe segurava os braços, e ninguém notou o rosto que
desfalecia. Assim foi a vida de Franz Kafka: ferido pelo aguilhão do
minotauro, não encontrou o rosto que pulsava e, ainda assim, se sentiu
culpado de não estar em casa:''

E o outro, cujo título poderia ser ``Teu lugar'', diz assim:

``Enquanto muitos sonharam com o dia inaugural, o grande dia em que o
homem, arrancado da imanência biológica, disse o mar e a flor pela
primeira vez, eu, desde sempre um habitante nu dessa hora misteriosa,
desprezei o dom confiado e sonhei em vestir a máscara do animal
racional.''

Gosto dos aforismos de \versal{JP}, pois eles mostram que ele passou pelo mesmo
lugar, comungou da mesma posição que eu. A~literatura é algo muito
diferente, é algo mais sério do que o ``literário'' e o ``estético''.
Ela é uma questão topológica, uma questão de passar pelo lugar de onde
brota um dizer. Penso que a condição para me encontrar é partilhar de
uma fragilidade radical e uma exclusão do mundo instituído. Assim \versal{JP}
pôde me encontrar porque a criança \versal{JP}, o menininho \versal{JP}, já se encontrava
inteiramente atravessado e precedido por uma experiência kafkiana.

\versal{JP} estava ainda no pré"-primário e vestia um uniformezinho azul quando a
professora carimbou bichinhos e figurinhas no canto direito da página do
caderno para que as crianças escrevessem frases a partir das figurinhas.
Ora, como as figuras estavam carimbadas na extremidade direita da
página, \versal{JP}, talvez para honrar a proximidade dos bichinhos, escreveu
todas as frases ao contrário, indo da direita para a esquerda, e não da
esquerda para a direita, como seria o correto. Escreveu tudo ao
contrário e foi repreendido. Ficou muito assustado, pois lhe parecia
aleatório demais fixar a direção correta das frases. Diante daquela
violência e da aleatoriedade encontrada, \versal{JP} foi se tornando um simulador
assustado e, por assim dizer, passou a vestir a máscara do animal
racional a fim de sobreviver naquele lugar completamente diferente do
que lhe seria hospitaleiro. \versal{JP} viu"-se obrigado a simular uma competência
dentro do elemento escolar, elemento cujo nome é abstração…

O problema é que o progresso na abstração, o grau de distanciamento em
relação ao sentir ressoante do corpo na proximidade das coisas
concretas, constitui o chamado do mundo e o chamado escolar. O~domínio
de caixas cada vez maiores é a própria escalada da abstração e do
poder… E é um tal domínio que a escola pede de um menino! Vem
para a caixa você também, caixas cada vez maiores e mais poderosas!!
Ora, a força desse chamado puxava o menino numa direção que roubava a
possibilidade do nascimento do menino"-concreto. Onde e com quem poderia
ele nascer e existir? Onde encontraria alguma cidadania o menino que
olhava os mapas e pensava que a Argentina era laranja e roxo o solo
afegão? O menino que queria parar o carro do pai para pegar nas mãos a
linha do trópico de Capricórnio? Não havia mais mundo, não havia
território para o menino corpóreo; ele teria de seguir errando em puro
vácuo, simulando um intelecto e um \versal{QI}, memorizando loucamente equações,
com medo que tudo desmoronasse no instante seguinte. Quando vejo esse
menino vagando por corredores e recreios, vejo"-o sempre assustado, com
medo que descubram a fraude. Não esta ou aquela fraude específica, mas
ele mesmo, o \versal{JP} inteiro como fraude! Ele mesmo enquanto caminhava pela
sétima série, por exemplo, e já no alto de uma grande caixa, se ele
fosse detido e testado ali, a fraude seria revelada, pois ele não
poderia estar ali e teria de, por exemplo, ser reenviado para o primário
ou para uma escola de anormais.

É óbvio, entretanto, que o processo de engolimento e de destruição do
ser de \versal{JP} pela hegemonia da abstração não começou na escola. Começou bem
antes num outro lugar chamado lar. As pessoas ainda pensam que nas casas
há obscuridade e concretude suficientes para um ser humano desabrochar,
quando, na maioria das vezes, a casa já se tornou igualmente abstrata e
o seu cotidiano já não nasce nem se sustenta no domicílio de dois corpos
que se amam e se indagam, e no excesso e no gozo por meio do qual um
novo ser aportou na terra, mas na mera funcionalidade de um amor"-morto
entre dois seres igualmente abstratos…\footnote{Eu fui um dos primeiros a perceber esse processo de invasão da técnica
na província familiar. Posteriormente, Jünger, Heidegger e os
frankfurtianos tentaram compreender o que para mim era pura experiência,
puro padecimento.}  Para
usar as palavras de Thomas Bernhard, eu diria que realmente nos
encontramos com mães industriais e pais industriosos, ambos mutuamente
abandonados e incapazes de sentir o apelo da concretude, de sentir a
vibração e o chamado da concretude"-filho. Porque, na verdade, quando uma
mãe industrial encontra uma criança recém"-chegada, a criança vê"-se
rodeada e invadida por uma produção incessante de mundo, de gestos e de
palavras, e toda essa produção, precisamente na condição de produção,
não emerge do corpo da mãe, mas dos saberes técnico"-científicos que
absorveram e engoliram a opacidade e o vazio da mãe enquanto mãe. Nesse
sentido, pode"-se facilmente dizer que a mãe industrial é aquela em que a
mãe real, como nos filmes de \versal{ET} e de science"-fiction, foi roubada e
destituída pela competência tático"-gerencial da mãe industrial. Muitos
dirão que ainda não adentramos num tal distanciamento abstrato, dirão
que a experiência de \versal{JP}, bem como a minha, 40 anos antes, é da ordem da
patologia. Entretanto, toda e qualquer patologia acontece nesse mundo e
é obra e janela para a verdade desse mesmo mundo. Se o caso de \versal{JP} é
patológico, é porque ele enuncia e revela, na mesma medida, o crescente
adoecimento do mundo. Parece"-me bastante óbvio que se um garoto
desaparece porque é engolido pelo minotauro técnico, isso diz apenas de
um planeta esquadrinhado e morto, de onde se bane progressivamente o
festim indomável que é o homem.

Se \versal{JP} nasceu no planeta implacável da medida, e até brincadeiras de um
longe"-abstrato e pedagógico destinavam no à perfeição e à assepsia, sua
sorte foi não ter conseguido participar desse mundo. Sua sorte e
privilégio foi ter estranhado e ter ficado excluído desse mundo que se
apresentava como sua casa. E~o que era esse estranhamento senão uma
saudade e uma espera da terra viva e de um encontro amoroso sem medida?
Assim, tudo o que vinha do mundo o enchia de pavor e estranhamento. Ele
não grudava nem se identificava com nada. Ensinavam"-lhe as palavras e
ele passou a picotá"-las e a dividi"-las. Se alguém dizia: ``Me chamo
Solange'': imediatamente ele separava as vogais das consoantes e fazia
um campeonato numérico: \versal{SLNG} versus \versal{OAE}, 4 a 3. Diziam: amor e ele
picotava em \versal{AO} versus \versal{MR}, 2 a 2. Fazia loterias numéricas e dizia as
palavras e as frases de um lado para o outro, de frente para trás e de
trás para frente. Ele assistia toda a linguagem de fora precisamente
porque não podia aceitar a linguagem"-aprendizado, a linguagem"-abstrata
do mundo, e ansiava pelo fogo quente da linguagem que brotasse do seu
corpo atravessado pelo mistério das coisas.

Mas a língua"-viva, a língua"-concreta estava interditada em todos os
lugares e \versal{JP} atravessou todo o período escolar fazendo ginásticas
verbais e roubando relatos. Prosseguia picotando e soletrando, pois
assistia cada palavra, cada ``produção linguística'' com a angústia de
saber"-se fraudulento. Ele era um animal assustado decorando dicionários
para não ser aniquilado e, entre o horror da fenda e a claridade dos
nomes, ia tentando manter o disfarce da normalidade, a máscara do animal
racional.

Um belo dia, no início do segundo colegial, algo extraordinário
aconteceu: um professor de língua portuguesa começou a cumprir o
programa de literatura e, tendo dividido a classe em vários grupos,
designou para cada um a leitura de um clássico da literatura brasileira.
\versal{JP} ficou sem grupo e, sozinho, viu"-se incumbido da exposição do livro A
Carne de Júlio Ribeiro. Era uma quarta"-feira fria e o céu estava aberto
e límpido e, quando \versal{JP} subiu ao tablado para iniciar sua apresentação
oral, ele viu algumas nuvens correndo e fugindo de outras nuvens. A~classe era uma imensa algazarra, uma vez que as aulas de humanidades
eram consideradas pelos alunos e pelos próprios professores uma
perfumaria para afeminados, sendo as ciências exatas o verdadeiro lugar
e a verdadeira sede da inteligência. \versal{JP} começou a falar, começou a
recontar a história do livro A Carne de Júlio Ribeiro e a algazarra
começou a se desfazer. De um modo completamente inusitado a balbúrdia
foi diminuindo, os papeizinhos atirados foram ficando mais rarefeitos e,
quanto mais \versal{JP} falava, mais a classe se comprimia no silêncio, até um
ponto em que o próprio professor começou a ficar surpreso com o que
estava acontecendo. Depois dos primeiros 20 minutos, a classe parecia
úma missa"-solene e a palavra jorrava da boca de \versal{JP} completamente
borbulhante e incalculada como se ele mesmo fosse um gêiser da
Groenlândia ou do Parque Yellowstone. \versal{JP} não entendia como ele podia
estar tão calmo e tão pouco massacrado!! As palavras vibravam e saíam
por sua goela numa festividade sem precedentes e, quando ele relatou um
suicídio arrependido e um ato sexual, a classe havia se convertido numa
estátua e \versal{JP} parecia uma onda quebrando na areia de uma praia intacta.
Ao terminar, após uma hora de narrativa ininterrupta, os alunos diziam:
``Car"-r-ne, car"-ne''… e, durante os meses subsequentes, quando \versal{JP}
entrava na classe ou quando o professor chamava um novo grupo ---
``Hoje, seminário do Casa de Pensão de Aluísio Azevedo, hoje seminário
sobre Dom Casmurro'' ---, os alunos diziam: ``car"-r-ne, A car"-ne''!

Depois do seminário sobre Álvares de Azevedo, \versal{JP} e três colegas
começaram algumas reuniões literárias batizadas de Epicurreia. Eles se
reuniam na casa de algum deles e, depois de tomarem vinho e lerem algum
poema, eles se questionavam mutuamente por escrit0: ``O que você diria
para uma plateia de suicidas?''; ``Qual o sentido da vida?''; ``O que é
escrever?'': As perguntas eram respondidas por escrito e depois lidas em
voz alta. \versal{JP} ficava extasiado e para ler suas respostas subia em cima da
mesa e dos móveis. Foi na sétima e última Epicurreia que ele recebeu de
um colega um exemplar de A Metamorfose. Na mesma noite, \versal{JP} leu as
primeiras páginas de meu livro e recortou a minha foto e colou"-a na
cabeceira da sua cama. O~vestibular se aproximava e o fogo da palavra
corpórea devia ceder lugar ao cálculo e às cópias mnemônicas. Da
cabeceira da cama eu vi \versal{JP} definhar novamente. Eu o vi sem lugar e sem
tempo… eu o vi perambulando na argamassa da palavra"-dita,
tentando decifrar o hieroglifo aleatório dos seres fictícios, tentando
imitar o deslizamento e a palavra"-esperanto dos seres
narrados"-fora"-da"-carne. Eu b vi emudecido diante do muro das medidas
(assustado, sentindo a solidão dos aposentos onde o poema não cresceu).
Foi dali, da cabeceira que eu percebi que cada fibra, cada átomo do ser
de \versal{JP}, o seu si"-mesmo, o coração da alcachofra \versal{JP}, estava contido numa
só frase: \versal{EMUDECIDO} \versal{ANTE} O \versal{MURO} \versal{DAS} \versal{MEDIDAS}. Uma \versal{SÓ} frase, uma \versal{SÓ}
experiência, mas como ela se abre para a imensidão! E é nessa imensidão
que eu me encontrei com ele, pois \versal{JP}, assim como eu, foi um ratinho
querendo escapulir, querendo atravessar o aposento cheio de esquifes e
ossos gigantes. Num segundo, num átimo, atravessar e pular pela janela
ou ficar congelado e aniquilado de pavor. --- Belo no homem é ele ser
uma desmedida: então, a palavra visita a sua boca, pois ele está exposto
ao milagre do mundo!

\section{\versal{CONTRABANDOS} E \versal{APROPRIAÇ}Õ\versal{ES}}

\begin{itemize}
\item
  \versal{BLANCHOT}, Maurice. \emph{La Risa de los Dioses}. Madri, Taurus, 1976.
\item
  \versal{SAFRANSKI}, Rudiger. \emph{Quelle Dose de Verité les Philosophes
  Peuvent"-ils Supporter?} Paris, Presses Universitaires de France, 1993.
\end{itemize}
\section{\versal{REFER}Ê\versal{NCIAS}}

\emph{De e sobre Kafka}

\begin{itemize}
\item
  \versal{ANDERS}, Günther. \emph{Kafka: Pró e Contra}. São Paulo, Cosac Naif,
  2007.
\item
  \versal{CITATI}, Pietro. \emph{Kafka}. Paris, Gallimard, 1990.
\item
  \versal{CALASSO}, Roberto. \emph{K}. São Paulo, Companhia das Letras, 2006.
\item
  \versal{FLUSSER}, Vilém. \emph{Da Religiosidade, a Literatura e o Senso de
  Realidade}. São Paulo, Escrituras, 2002.
\item
  \versal{KAFKA}, Franz. \emph{O~Castelo}. São Paulo, Companhia das Letras, 2000.
\item
  \_\_\_\_\_\_\_\_. \emph{O~Processo}. São Paulo, Companhia das Letras,
  2005.
\item
  \versal{KUNDERA}, Milan. \emph{Os Testamentos Traídos}, ensaios. Rio de
  Janeiro, Nova Fronteira, 1994.
\item
  \versal{LEMAIRE}, Gérard"-Georges. \emph{Kafka}. Porto Alegre, L\&\versal{PM}, 2006.
\end{itemize}
\section{\versal{LEITURAS} \versal{DE} \versal{JP}}

\begin{itemize}
\item
  \versal{AZEVEDO}, Álvares. \emph{Poesia}. Rio de Janeiro, Agir, 1957.
\item
  \versal{RAMOS}, Graciliano. \emph{São Bernardo}. Rio de Janeiro, Record, 1978.
  \versal{RIBEIRO}, Júlio; A Carne. Rio de Janeiro, Ediouro, 1978.
\item
  \versal{KAFKA}, Franz. \emph{A~Metamorfose}. São Paulo, Companhia das Letras,
  1997.
\end{itemize}
\section{\versal{OUTRAS} \versal{REFER}Ê\versal{NCIAS}}

\begin{itemize}
\item
  \versal{ANJOS}, Augusto dos. \emph{Obra Completa}. Rio de Janeiro, Nova
  Aguilar, 2004.
\item
  \versal{LUKÁCS}, Georg. \emph{História e Consciência de Classe. Estudos sobre a
  Dialética Marxista}. São Paulo, Martins Fontes, 2003.
\item
  \versal{SIMMEL}, Georg. \emph{Philosophie de l'argent}. Paris, Quadrige/\versal{PUF},
  1987.
\end{itemize}
