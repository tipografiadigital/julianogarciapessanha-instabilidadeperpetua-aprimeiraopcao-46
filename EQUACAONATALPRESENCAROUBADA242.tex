\chapterspecial{Equação Natal: Presença Roubada}{}{}
 \epigraph{Pequena ilha ao leste do mar Brinco à luz da areia com um caranguejo A
face molhada de lágrimas.
\versal{TAKUBOKU} \versal{ISHIKAWA}}{\versal{TAKUBOKU} \versal{ISHIKAWA}} 

Eu sou um homem sob medida. Eu caminho no interior de uma moldura. Eu
não aconteço, eu me assisto. Eu não fluo e o desconhecido não cerze
caminhos através de mim. Cada gesto que vou ``executar'' já está
narrado, pensado. Vivo num mundo sem irrigação, sem veia e sem fluência,
antimundo onde nada é livre, onde nada ficou encoberto. Apagou"-se o
bruxuleio da última chama. Um olho biônico invadido em mim antecipa em
pensamento o sentido de cada gesto e de cada palavra que dirijo aos
outros. Isso destrói e apaga a rugosidade da vida, sua face opaca,
transformando tudo em significação pura, em ``pura significação''. Tudo
agora está liso e eu funciono, eu transito. Não é espantoso?! Às vezes
eu penso: quer dizer então que acabei por exorcizar o componente de
opacidade que alegra e mina a existência de cada homem? Desfinitizei"-me?
Por componente de opacidade entendo o fato de que as significações dos
gestos e atos de alguém lhes são, em grande parte, ou desconhecidos ou
conhecidos apenas posteriormente. Isto é, há um hiato entre o gesto e o
pensamento, e o gesto excederia o que pode ser pensado no pensamento,
mas, no meu caso, a transparência do sentido, sua anterioridade total e
sua incrível saturação, já coincide e já antecipou o gesto que realizo.
Sou um homem deduzido, não um homem inaugural. Penso então que minha
vida desconheceu a intensidade e o arrebatamento, e que, então, talvez a
criança que fui inexistiu. Ou ela jamais teve gesto ou, de algum modo,
esse gesto lhe foi roubado. Caiu muito asfalto, caiu piche no braço e no
rosto de uma criança. Pois, como é possível uma criança sem gesto? Teria
de haver uma criança sem corpo. Mas o que é isso então? De qualquer
modo, o mundo me parece invertido agora, pois é o pensamento que legisla
o gesto e a ideia que comanda o corpo. Trata"-se de uma existência em
segundo grau, tísica mesmo, uma existência em pensamento, pois no adulto
que sou aniquilaram"-se sombra e corpo, e eu apenas dou voltas nesse
reino ofuscado; reino de autotransparência. Já não tenho nem sonhos,
desconheço o milagre das cifras noturnas e eles (os sonhos) são meus
velhos teoremas de acordado. Eu sou uma vigília permanente, observo tudo
mesmo quando estou dormindo.

Mas eu não sofro de auto"-observação. A~auto"-observação implicaria em
pensar que algo jorrasse antes para ser depois observado. Haveria uma
precedência do gesto e da opacidade inomeada sobre o que é, além,
clarificado. Mas não é assim que me acontece. Os nomes precedem tudo. Os
conceitos estão grudados em mim parece que desde sempre. Eu me fabrico a
partir de conceitos. Ser é igual a ser autoproduzido a partir da ideia.
Por isso já anotei: ``Eu nunca passei de cibernética, de imperador do
vácuo e do barroco. Eu sou o estrategista de mim mesmo, artista da fome
e fracasso da verdade'': Portanto, não há auto"-observação, mas uma
autofabricação avassaladora, uma simulação permanente de presença, uma
simulação performática de quem teve a primeira presença roubada. Mas há
uma primeira presença? E o que é uma presença primeira roubada? É isso
possível? Então eu não nasci? Quem sou? Às vezes eu vejo que fui
plantado direto no lagos, plantado no pensamento, em frases longas e em
frases curtas. Tornei"-me uma astúcia desvairada, um gigante do
speech"-act… mas se é assim, se eu sou apenas um Flipper
linguístico, ladrão de frases e relatos, o que teria ou não teria
acontecido no velho país da infância?

Um dia eu considerei conveniente interrogar de que pânico, de que núcleo
assustado toda essa construção pretendia me afastar! Esse movimento de
pura claridade (esse universo de significações decalcadas); seria ele um
afastamento, um terror de transbordar? Mas se afastar de quê?
Transbordar de onde? De quê? Teria de haver, então, algo antes, alguma
região anterior, algum espaço ou experiência dos quais eu fugisse. Mas
não havia nada disso antes. E, aliás, é esse exatamente o problema:
devia ter havido algo antes, mas eu fui plantado direto na realidade
metafísica. Um homem plantado direto na objetividade. Essa aberração é
cada vez mais comum e ela foi profetizada já por Dostoiévski no final
das Notas do Subterrâneo. O~homem, a planta"-homem, deve enraizar"-se
antes na nascente suave do real, na delicadeza de um brotar e de um
borbulhar para então, e só depois, colocar cabeça e corpo no
mundo"-realidade. Antes do mundo liso da instituição há o mundo nascente
do poema. Todo homem deve passar pelo sopro do poema; poema em que o
gesto da criança fez"-se um no rosto daqúele que o festeja. Quem não é
saudado no interior do brilho apaga"-se e é reengolido na garganta do
vazio. Só a circulação de uma primeira intensidade desfaz a onipresença
da fenda. Então eu pergunto: onde eu não enraizei? Por que eu não
enraizei? Será que não havia ninguém lá? Como estavam eles quando
cheguei na terra? O que aconteceu para que eu ficasse engolido e
destinado à migração contínua pelos penhascos da fenda? Ah, humanidade!
Como eu te pressenti, e, da lonjura mais distante, como eu sonhei com os
teus espaços! Eu me tornei um devoto do poema desacontecido e um
simulador de humanidade. Desde muito cedo aprendi a passear na ``tua''
corrente sanguínea. Privado de uma história possível, tolerável --- pois
toda história começa no poema ---, pegava carona nas histórias alheias.
Em quantos bairros e cidades não fui parar dizendo as formas da ``tua''
vida? Quanto tempo escutando --- até o dia da monotonia?! --- essa
intensidade estranha que é um ser humano. Minha inconsistência (o pai de
santo, o vidente) ouviu a voz de tantas consistências. O~que era o
outro, afinal? Que tipo de consistência é um ser humano? Eu via a
humanidade subjetiva, o sujeito constituído almoçando no imenso
refeitório. Cada um, solitário, comia o bife de seu próprio prato. Nas
pausas trocavam palavras anêmicas. O~bife, o prato, é a história, a
narrativa de cada um. A~história pessoal era o claustro, o claustro de
uma referência que cegava para o enigma de tudo. Pensei que ``ter
histórià: ser alguém, era transformar tudo na ``terra batida'' de um
conhecimento… Eu sempre olhei essa mesa de longe. Eu vi o que
havia em cada prato, mas, ao olhar para mim mesmo, não havia nada, prato
algum. Eu não existia em mim mesmo e o meu brinde, o brinde do excluído,
é o olhar. O~olhar abismal é o dom do abandonado. Ele não vive na ilusão
de estar vivendo por Dentro e não tem narrativa que o localize. Para que
alguém se assente numa narrativa, é necessário, antes, ter passado pelo
poema. Basta olhar. Uma criança faz gesto. Ao ver céu e montanha, ao ver
o animalzinho correndo, ela ergue os braços em perplexidade. Ela está
tomada pelo enigma. Eis aí um início e um ancoradouro. Se alguém esteve
lá com a criança, ressoando a vibração do enigma, então há um salto, a
criança pode dançar e migrar para além da fenda, para o interior da
descoberta de que as coisas são! Mas é preciso que essa intensidade
tenha circulado, tenha se esparramado mutuamente, tenha ido e voltado do
rosto do outro até o da criança. O~espelhamento da intensidade da
descoberta, essa partilha é o amor. O~amor é comunhão na perplexidade,
partilha do enigma de que estamos descobrindo o mundo. Estamos
descobrindo o mundo, pois tanto o recém"-chegado quanto o anfitrião devem
estar no mesmo espaço. Tudo aquilo que brota na criança e através da
criança, o arrepio da montanha e da praia, o vento de que tudo isso é,
deve estar no rosto da mãe. Ela não domestica; 1. presença, ela abre
mais reverberando. Sustenta o brilho do acontecer. Mas se o poema morreu
dentro do adulto, se ele já fechou o seu olhar de aparição, então o que
é aberto vai ser logo fechado, selado. E~a ventania do novo visitante é
respondida com um traque de ventilador. Então o rosto não desabrocha, a
criança não continua, fica sem tempo, sem lugar, vai zanzar na
vergonhosa zona pré"-natal e tentar enviar seus embaixadores da luz, seus
personagens, suas coreografias fatigadas.

Eu mesmo, quando recordo e soletro meu país natal, percebo que a criança
que fui não teve continuação e que a minha dança ficou interrompida; e
quanto mais eu me aprofundo nessa investigação natal, mais eu encontro a
frieza do meu tempo e de seus saberes e mais eu sou obrigado a narrar a
implosão e a invasão da província humana. Quando eu nasci, o mundo
estava muito adiantado e os seus tentáculos já haviam engolido quase
tudo. A~espiral do progresso e a ideia moderna de liberdade haviam
arrastado meu sistema familiar numa tal vertigem que a criança
recém"-chegada passou inteiramente despercebida. É~como se eu pretendesse
pôr os pés no chão para tocar uma nuvem e eles estivessem voando, voando
em narrativas de avião; tudo solto, tudo cifra, tudo ficção, tudo no ar
de uma enorme Disney"-fiction. Mas uma tal obstrução de encontro não é
assunto psicológico, pois já, há muitas gerações, que se pretendia
erigir a casa humana no além e no suprassensível e, mais recentemente,
pretendeuse construir o suprassensível aqui mesmo na terra com a ajuda
da ciência e da medicina. E~foram elas que me saudaram, foram as
mensagens delas atravessando os genitores engolidos que saudaram o bebê
sabor morango. Eles estavam lá, mas em pleno sumiço do corpo. A~terra
estava lá, mas diluída milhões de vezes na poção do objeto. Até uma
mamadeira estava lá, mas ela tinha asfalto e cortisona junto do leite.
Palavras, também palavras e falas estavam lá, mas de nada adianta haver
palavra se já não há real a recolher e a·dizer. Então, eu desertei para
o interior da fenda"-fora e nada abandonei na deserção, pois percebi que
o meu primeiro nascimento não seria suficiente. Não fosse a proteção do
estranhamento, não fosse a alergia à onipresença da luz e do barulho, eu
teria me tornado um estilista do cabelo azul e estaria hoje tão grudado
e integrado neste mundo que eu jamais poderia vislumbrar a frieza do
primeiro lance. Eu seria, então, apenas uma aberração tranquila e sem
pergunta. Ao contrário, a repelência que me desintegrou do mundo e a
alergia que me fez recuar até o coração do exílio eram já uma pergunta
ardente: onde está o poema da chegada? Tua vida, onde fica? Mas houve
isso, uma vida, se ela jamais esteve no poema?

Certa vez eu caminhava por um bairro de minha cidade quando vi uma
criança ení cima de uma casa. Ela vestia máscaras de monstros. Tirava
uma e colocava outra. Percorria o teto aos gritos, se escondia e
reaparecia. Eu fiquei atrás de uma árvore e vi que ela tentava inquietar
um velho transeunte. Era como um pedido de socorro, uma confirmação da
sua presença. Mas o velho passante, naquela tarde fria, naquela rua de
casas ricas que eram templos vazios, o velho passante estava
excessivamente gravitado em memórias e projetos. Era um sonâmbulo
entretido nos pequenos astros de sua psique. Não soube acolher o grito
do garoto, não soube lançar um olhar"-de"-uivos que pusesse no menino
alguma nuvem de espessura. Eu ruminei por muito tempo o acontecimento
desse garoto no telhado. Eu soube que ele noticiava o impossível, a
imensidão vazia, e que não haveria passagem para o rosto ou para o lado
interno da existência sem o abraço de um poema partilhado. Eu continuei
andando, eu atravessei um longo canteiro de flores esquecidas, eu pensei
que ali estava contida a primeira e a grande desconfiança quanto ao
estatuto do que é. Compreendi que ele havia nascido num lugar imenso
cujo nome é satanismo da luz. Falo sobre esse lugar. Falo da solidão na
claridade. Falo do recém"-chegado em sua delicadeza sem forma encontrando
um excesso de forma. Falo de um olho fascinado pela aparição encontrando
o olho gelado do cálculo. Falo da dor desse desencontro. Não é uma
escrita subjetiva, autobiográfica. Só existe ``eu'' no lado de dentro do
mundo. Eu fiquei de fora, fiquei muito longe, numa lonjura que nenhum
astrofísico calcula. E~nessa lonjura sonhei e proclamei o poema. A~incandescência do corpo que não houve. É~esse lugar que fala. Eu sou
apenas seu mensageiro. O~bebê que fui aposentou"-se para o mundo logo
cedo. Eu sou a antimatéria do mundo, tudo que é arranjo e trançado
mundano cai em mim num buraco negro. O~instituído e a história se
nadificam. Eu sou apenas o sonho e a saudade do poema originário, por
isso não há eu algum quando falo. Há apenas o desfazimento de todo
calcário, de toda identidade acolchoada e o clamor do fluxo, da cascata
acompanhada até a nascente.

Mas às vezes é tudo ao contrário, o pêndulo pende para o outro lado e eu
já não sou mais o buraco negro do mundo, mas é o mundo o buraco branco
que me captura inteiro. Nesse esticar"-se pendular de um extremo ao
outro, nesse dilaceramento está o combate. A~gravidade do combate. Então
um único nome me aniquila. O~calcário do mundo me esmaga. Sou o lugar da
luta entre o fluxo e o calcário. Muitas vezes já não sei se há o sopro
da palavra se inscrevendo em mim ou se é apenas um escritor, um
estilista do cabelo azul, simulando o assunto do limiar e do extremo do
humano. Agora mesmo, no preciso momento em que redijo estas notas, me
sinto acossado pela dúvida, me sinto soterrado debaixo do calcário.
Escrevo sendo filmado e esquadrinhado pela medida opressiva de duzentos
olhares e duzentas vozes, então um frio de horror se aloja no meu peito.
É~o terror da falsidade. A~desconfiança permanente por ocupar um lugar
tão frágil, pois o que pode uma nascente no mei do asfalto? Onde brotar
se já quase tudo está extinto? Vejo de um lado todo o palácio de cristal
da instituição literária e, do outro, a pequena fagulha do poema. Pode
um camundongo atravessar um elefante, entrar"-lhe pelo rabo e furar toda
a sua pele grossa para saltar do outro lado pela tromba? E já nem se
trata de elefante. O~elefante é um animal maravilhoso. Se trata de
manadas inteiras de entulhos e de carapaças de metal e de calcário. Se
trata de um universo inteiro hipernomeado de sentido, hipersaturado de
narrações. Escrevo para reencontrar a primeira intensidade, a
intensidade sem sentido do gesto inaugural. Escrevo para furar e
atravessar o piche sobre o meu rosto·e os meus braços. Preparo o meu
corpo para o poema da proximidade. Escavo o lugar de um novo começo;
lugar de um rosto que amasse o fundamento. No meu primeiro começo a
intensidade não circulou, ela teve de retroceder. Estancou. A~festa da
criança ficou sem ser inaugurada e os meus mares ficaram sem praia.
Aconteceu de eu caminhar por uma praia quando vi um caranguejinho azul.
Ele zanzava loucamente de um lado para o outro e eu ergui os meus
bracinhos para comemorá"-lo; e dizendo ``bis"-su, bis"-su, bis"-su'':
esfreguei os pés na areia num transe de imitação e de excitação diante
daquela estranha criaturinha. E~essa perplexidade maravilhosa já ia se
convertendo numa espécie de dança sacra quando um membro do meu sistema
familiar decretou: ``Esse menino vai ser doido, ele é meio psicótico''.
Então uma única sentença estuprou a dança do menino. Porque os
funcionários do meu aparelho familiar já viviam tão longe do poema, não
puderam juntar"-se ao menino e com ele avançar até a região comovida onde
o homem agradece a maravilha de haver coisas. Para tanto, para que eles
tivessem acolhido e reconhecido o meu gesto, eles teriam de ter, ainda,
alguma companhia do desconhecido. Era preciso, ao menos, ter um pouco de
caos dentro de si. Mas não! Uma imensa claridade os havia transformado
em tautologia apagada dos saberes. E~esses saberes que eram saberes do
progresso e da ascensão tinham ocupado e narratizado todos os espaços
dentro das entidades familiares. Já não havia restado nenhuma região
intacta, nenhum lugar vazio. A~festa da criança ficou sem o tu e sem o
outro. Os anfitriões não dançaram um ``Olha, um caranguejo!'': um
``Vixe, um caranguejo!''; não disseram uma palavra"-gesto, uma
palavra"-corpo para saudar o movimento livre. A~festa da criança não
conseguiu arrastá"-los até a ventania da aparição. A~criança ficou só e
desde esse dia a intensidade e o mundo se tornaram líquidos imiscíveis.

Às vezes eu invejo aquele cujo gesto se misturou com outros corpos e
outros gestos ressoantes. Para esse, talvez, a intensidade pôde adentrar
no mundo. Comigo não foi assim. No meu caso, a equação natal se resume
de um modo muito simples. Atrás de mim o gorila da fenda me ameaçando e,
na minha frente, um peito de silicone e um diagnóstico psiquiátrico.
Nessa situação, é impossível não ficar atordoado. A~passagem da
fenda"-fora ao dentro pressupõe a mediação do poema corpóreo. Sem o
poema, sem o abraço físico junto de tudo que é terrestre, se você for
para trás o gorila te carrega para o abismo e, se você for para frente,
você é congelado pelo teatro figurativo da mulher moderna. Se você
recua, a fenda te engole, e se você avança, teu gesto é considerado uma
loucura. Essa constelação natal presidiu minha existência de tal modo
que seria tedioso narrar os episódios. Seria fermentar as palavras,
engordá"-las e, para não fazer isso, atenhome a um único acontecimento, o
da primeira namorada.

Eu já tinha uns 25 anos quando conheci minha primeira namorada. Eu
simulava idiomas amorosos, me esforçava para ter algum poder no mundo e
para me tornar um ``eu'' dotado de alguma competência… quando
notei que ela estava indo embora. Imediatamente desabou a competência
estética, os trejeitos fabricados de homem"-viril e a competência
intelectual, os jargões imitados da filosofia, e eu emudeci
completamente. Ela partiu e todos os cálculos positivistas de
distância"-abandono e todas as pequenas ressurreições esporádicas diante
do sorriso dela, tudo isso submergiu e desapareceu num buraco
gigantesco.

Com a queda dos ``bonecos defensivos'': o estético e o intelectual,
vaguei dois meses engolido no vazio. As palavras já não grudavam em nada
e eu estava exausto de dor. Foi então que numa noite, trazido
misteriosamente pelos dedos invisíveis do acaso, deparei"-me com o livro
Notas do Subterrâneo de Fiodor Dostoiévski. Eu o li de um só golpe e, ao
terminar, percebi meu coração pulsando quase humanamente. Beijei a
lombada do livro e anotei na contracapa: Você é o grande livro da terra.
Você morou no quarto giratório das tremendas ansiedades. Você beijou a
goela do horror e, mesmo assim, me parece que, certa vez, a vida de um
homem esteve protegida pelo véu da noite… Peguei, então, o livro
e saí correndo --- meu coração batia mais rápido que o de um pardal que
tivesse fumado quatro carteiras de cigarro --- e fui até o apartamento
da ex"-namorada. A~moça assustou"-se comigo e tentou me expulsar, mas eu
disse a ela que era necessário , que ela escutasse a Coisa. Abri, então,
a segunda parte das Notas do Subterrâneo e li em voz alta durante duas
horas. Ela escutou atentamente minha leitura, uma leitura apaixonada e
mediúnica, quase psicofonante. Ao final, ela me abraçou e disse que era
lindo, que era excessivamente humano e que, já que ela amava o humano, o
seu amor por mim também era incondicional! Eu estava felicíssimo,
parecia acordar de um pesadelo e relaxei a tal ponto que acabei
adormecendo no colo da moça.

Entretanto, essa foi a última vez que a vi. A última vez que ela me viu
ou ouviu. Quando acordei, ela tinha desaparecido. Um ano e meio depois,
na segunda vez que abandonei meu quarto pela insistência de um amigo,
fui parar num bar de universitários, e lá, de repente, escuto na mesa de
trás uma voz familiar: ``Ah, sim, mas isso foi há muito tempo atrás,
tanto tempo que eu já quase nem enxergo. Parece um desses sonhos que não
têm realidade'': E depois veio a voz de um cara: ``Mas o camarada te lia
Dostoiévski, todo suado e descabelado, e ainda dizia parecer"-se com ele,
reconhecer"-se nele? De que estranho período é esse cara? E como é que
era o rosto do idiota?'': Ao que a mulher acrescentou: ``E eu então, que
dizia haver uma humanidade imorredoura, uma condição humana na qual o
paspalho dizia se reconhecer!''. A~moça ria, ria fundo, acho que
chegavam a saltar os perdigotos… ``Imagine só, essa velha
retórica cristã da angústia e do vazio! Já faz tanto tempo o do
psicólogo russo e, mesmo assim, alguns ainda conseguem a façanha de ser
velhos, de ficar à margem desse nosso novo mundo'': Eu já estava
paralisado de pavor, mas, antes de fugir, ainda cheguei a escutar a voz
de um terceiro, provavelmente uma profissional"-psi: ``Eu tive um assim.
É~terrível trabalhar com esse tipo de esquizopatia… mas logo"-logo
a família esconde em Ibiúna ou Caraguá, quando têm algum
dinheiro…''.

Eu fugi e, ao chegar em casa, escrevi sem parar e, ao escrever, ao
relatar o acontecido, eu comecei a cavar um outro lugar, um lugar alem
da normalidade e além da exclusão. Era preciso imitar o animal
fascinante cujo nome é cupim. Eu estava soterrado num lugar morto e
medido. Tinha o corpo barrado e só podia me mover no formol e no
quadriculado. Não havia outra saída senão cavar. Aquele lugar não podia
ser a vida! Não podia ter faltado coração e combustível na criação!
Conduzido pelo pressentimento do espaço aberto, cavei por mais de duas
décadas. Interpelado pelo brilho das orlas e apaixonado pelo futuro,
cheguei um dia a colocar os pés na areia e a presença roubada foi,
enfim, devolvida. Aspirei o fervilhar da espuma entre a terra e o mar, e
o meu corpo urrou ressoando a exuberância do paradiso terrestre. Agora,
guardo comigo um grande segredo e nenhuma potência do mundo conseguirá
arrancá"-lo.

\section{\versal{REFER}Ê\versal{NCIA}}

\begin{itemize}
\item
  \versal{DOSTOIÉVSKI}, Fiodor. \emph{Notas do Subterrâneo}. Rio de Janeiro,
  Civilização Brasileira, 1986.
\item
  \_\_\_\_\_\_\_\_. \emph{Memórias do Subsolo}. São Paulo, Editora 34,
  1992.
\item
  \_\_\_\_\_\_\_\_. \emph{Notes from Underground}. New York/London, W.W.
  Norton \& Company, 1989.
\end{itemize}
