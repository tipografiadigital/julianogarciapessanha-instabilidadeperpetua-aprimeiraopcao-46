\chapterspecial{Mapas para o Atravessamento}{}{Cassiano Sydow Quilici}
 

Porque alguns livros são capazes de nos capturar, de nos puxar para fora
da hipnose dos hábitos e das ``sentinelas do conceito'', estabelecendo
conosco uma estranha conversa. Com esses textos podemos cultivar uma
espécie de amizade, que não nos confirma, mas nos lança diretamente na
urgência das questões. Este novo trabalho de Juliano Garcia Pessanha
chegou a mim como uma dissertação de mestrado. Com surpresa e alegria o
li, entre risadas e ``pensamentos estremecidos''.

As reverberações da leitura permanecem. Talvez porque não se trata de um
discurso que ``fala sobre'': que constrói belos desenhos em torno das
ideias e dos autores da hora. É~uma escrita que nasce do contato com a
``ferida'' do estar vivo, do aprendizado em sustentar um corpo exposto,
em luta com as armadilhas da abstração. A~palavra surge, ao mesmo tempo,
como vestígio e potencialização da experiência. Aquele que a profere se
encontra ``em jogo'': na fogueira do acontecimento. O~escritor torna"-se
assim, também, uma espécie de ator (no sentido artaudiano), ``que emite
sinais em meio às chamas'': Escritura teatral e performática: Juliano
transfigura"-se, em parte, nos autores que lê, para ser perpassado e
transformado por eles. Do mesmo modo, ele os transforma e os oferece a
nós. E~deste modo somos atingidos.

A erudição (literária e filosófica), também tão presente, não é nunca a
avalista dô que é dito. Triturada e misturada com acontecimentos físicos
e biográficos, torna"-se parte de uma estratégia, instrumento para a
travessia do mundo instituído. O ``pensamento topológico'' que Juliano
nos propõe é enfrentamento da violência de uma época, em que nascemos já
no processo de absorção do homem pelos poderes e saberes do mundo
técnico. Para ingressar no mundo, devemos nos tornar ``habitantes da
representação'': Os que, de algum modo, foram visitados e marcados pela
``perplexidade do estar vivo'': pelo ``assombro da aparição'' (e também
da desaparição), raramente encontram acolhida, correndo sempre o risco
de sucumbir num limbo. Ao pensamento e à poesia caberiam então a tarefa
de mapear os múltiplos modos do nosso enclausuramento, forjando
possibilidades de travessia e reinvenção.

Porque nascem de uma urgência, os ensaios deste livro apresentam uma
singular coesão. Não se trata de um desenvolvimento lógico e linear dos
temas, mas da exploração de caminhos, que criam entre si múltiplas
ressonâncias. Os vários autores tratados (Kafka, Dostoiévski, Fernando
Pessoa, Nietzsche, Heidegger, Artaud entre outros) são convocados como
parceiros, iluminando diferentes problemas e estratégias de
enfrentamento da nossa época. Se Kafka contribui como escritor que
melhor ``nomeou o nosso mundo inóspito'': Nietzsche deve também ser
chamado para que revele o apelo do ``além homem'' e possibilidades de
desmanchamento do eu que possibilitam a singularização.. São as
necessidades do nosso próprio ``atravessar'': que nos fazem encontrar e
estabelecer um diálogo fecundo com os autores.

O poder de afetar desta escrita deve muito, também, à presença de um
agudo senso de humor. O~texto no desloca por diversos registros e planos
de referência, sem muitas cerimônias. Heidegger pode aparecer ao lado de
uma velha esquisita que assusta um menino, um artista pode virar o
``estilista do cabelo azul'': o próprio autor declara ter nascido entre
o ``peito de silicone'' e o ``diagnóstico psiquiátrico'': Formulações
concisas e sarcásticas explodem aqui e ali, puxando o tapete quando a
``crítica à metafísica'' começa a se levar a sério demais. E é aí que
Juliano mostra suas estratégias de bufão, exercendo um humor que
dissolve máscaras e ri de si mesmo.

Juliano não faz ``filosofia profissional'' nem ``literatura'': Seus
livros são experimentos que não se inscrevem num gênero definido. Criam
um terreno movediço, em que o próprio autor exercita o desmanchar"-se e o
refazer"-se. E~nessa ``instabilidade perpétua'' muitas questões surgem
para ainda serem tratadas. A~tarefa do ``atravessar'' sempre ultrapassa
o exercício intelectual e requer a mobilização de uma ampla gama de
recursos e descobertas. Assim a escrita torna"-se parte de uma poética da
transformação de si que é também ação no mundo. E é nessa vibração que
ela pode nos atingir e conversar com nossa própria inquietude.
