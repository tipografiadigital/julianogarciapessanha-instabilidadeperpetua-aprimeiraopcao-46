\chapterspecial{Instabilidade Perpétua}{}{}
 \epigraph{Lá vem o vento correndo montado no seu cavalo Nas asas do seu cavalo vem
um mundo amanhecendo vem outro mundo morrendo Ligando um mundo a outro
mundo vem um grito reboando reboando, reboando.
\versal{JORGE} \versal{DE} \versal{LIMA}}{\versal{JORGE} \versal{DE} \versal{LIMA}} 

\emph{Para Carolina Wittmann}

Falo de uma topologia. É~uma ideia, desde que se conceba ideia como um
cadáver da experiência. Ou melhor, uma ideia é uma espécie de lixo ou de
secreção de um trajeto ou de uma andança. Estamos indo, somos um foguete
ou uma hera se alastrando sobre um muro branco; então, alguém chega para
conversar, pergunta por onde andamos e o que brotou nessa andança e
dessa andança. É~nesse sentido que lhe contamos uma ideia: algo
recolhido e testemunhado num trânsito. Algo para ser comemorado entre
amigos. Ela serve apenas a isso! Não se autonomiza, não se destaca da
conversação. Quando isso acontece, quando ela salta para fora da
instabilidade perpétua, então ela vira ``ideia''; vira um furúnculo no
rosto ou uma espinha feia na testa. Irmã da desmedida, ela se converte
numa medida em torno da qual se aninham as sentinelas do conceito a fim
de estabilizar o trânsito ou deter o foguete.

Dito isso, nomeio a região topológica: há uma linha de horizonte, há um
desfiladeiro frágil entre o buraco negro, onde zanzam os abismais, e o
buraco branco, onde erram os figurantes. Entre os dois buracos situa"-se
a linha do acontecimento, linhaonde o arrebatamento físico, consonante
ao brotar incessante, encontra sua máxima absolvição e confiança. Essa
topologia é resultado de uma ação centrífuga que separa radicalmente
tudo que se encontra misturado. Ao separar, encontra tipos extremos e
esses tipos permitem pensar humanidades, culturas, pensamentos e
políticas. É~uma grade para ser jogada fora logo que o olhar se
aclimatou a todas as regiões e já não se encontra mais no risco de ser
engolido pelo buraco branco ou pelo buraco negro.

Homens do buraco branco são os cidadãos da legalidade metafísica, os
habitantes da representação e da palavra anticorpo. O~segredo desses
homens consiste em que vestiram o uniforme da identidade mundana e
acabaram por se confundir com ele. Esse uniforme, enquanto camisinha
gigante, blinda o corpo contra a visita apofática do buraco negro e
contra a visita epifânica da criança na corredeira, criança em estado de
milagre. O~homem uniformizado é um assustado, pois o abrigo na forma da
determinação identitária está sempre ameaçado pela latência dos chacais.
Como ensiná"-los a amar os chacais que estão à espreita?

Cito exemplos de uma tarde. Era num sítio e a luz dourada de cobre
atravessava tudo, manchava os muros e invadia as frestas, mas ninguém se
moveu e nenhuma palavra sanguínea honrou o sol declinante. Eu escutei:
``Filha, pegue o casaco que já é hora do pôr do sol'': Mas então, para
onde foi a luz dourada? E o ``intercâmbio resplandecente'' do corpo com
o sol? A questões como essas o cidadão engolido pelo uniforme teria de
responder: ``Nós fabricamos um inconsciente. É~a nossa vergonha, !llas
também nossa esperança. Ele é o lugar para onde passa aquilo que não
estamos à altura de experimentar. Na verdade, nossa neurose é o sonho
imposto no mundo do buraco branco. Como o buraco branco nos fixa na
identidade da função, da competência"-trabalho, temos de perseverar na
figura e blindarmo"-nos contra o fluxo do arrebatamento. Ele destruiria
tudo o que somos. Seríamos demitidos de nossa casa e nossa casa é o
buraco branco'':

Na mesma tarde, no mesmo sítio, um garoto olhou o rosto iluminado da
namorada e esse olhar era um olhar solar e todo o corpo do garoto
parecia querer saltar na direção do abraço, mas a namorada, diante
daquele olhar físico, perguntou: ``Eu tô com espinha?'': E essa era uma
pergunta"-caminhão. Vale dizer que enquanto o garoto corria numa
bicicleta debaixo dos álamos com vários ``coelhinhos de sol'' nos pedais
e no guidom, enquanto em sua boca brotava um sorriso, a menina"-caminhão
o atropelou. Caso perguntássemos a ela: ``Você não reparou no olhar do
seu namorado?'': ela responderia: ``Não, não reparei; não entro em
ressonâncias. Eu venho do buraco branco. Lá somos mágicos espíritas. Não
temos corpo. Sou uma garota deduzida e emoldurada. Ideias e narrativas
deduzem todos os meus gestos. Eles não nascem do corpo; não são
inaugurais. São fabricados a partir dos relatos do buraco branco. Eu
estou entupida por esses relatos. Eles me dão passagem por tudo. Não
quero silenciar e viver a partir do desconhecido de um corpo. Não
suporto que algo brote da região opaca e fértil da aletosfera. Eu seria
demitida da minha casa e minha casa é o buraco branco''.

No mesmo dia, no mesmo sítio, já tinha anoitecido, mas apareceu uma
outra notícia. Dois homens conversavam e davam muita risada enquanto
olhavam numa parede fotografias antigas de pequenos aglomerados humanos
em cidades do interior. Gente em volta de coreto e de igreja matriz.
Alguns nos olhavam das fotos. Eu observei os homens dando risada, mas,
de repente, percebi, bem na gravatinha borboleta de um deles, percebi
que a morte estava sentada no topo da gravatinha e ela olhava
constantemente tanto para o dono da gravata quanto para o seu
interlocutor. Ela os namorava incessantemente, piscando para um e para
outro, mas eles não a viam. Não correspondiam aos seus acenos.

Temer a morte e esquecê"-la é a certidão dos homens do buraco branco. Se
a vida, conforme escreveu um buraco negro chamado Fernando Pessoa, é o
estar numa estalagem esperando a diligência do abismo, então, enquanto
os homens do buraco branco entram para o hotel e desfazem as malas --- e
vão para o salão conversar, tendo inclusive fechado as janelas para não
escutar o apito do trem da morte ---, os homens do buraco negro nem
chegam a cruzar a porta do hotel. Ficam em pé, na estação, sem desfazer
as malas; sabem que o trem já está apitando. Homens do buraco negro são
os antípodas do buraco branco. Eles são imiscíveis, pois a consistência
metafísica do amparo no mundo desmorona completamente quando se abre um
buraco.

Recentemente assisti na televisão uma cidade inteira sendo engolida por
um misterioso buraco: casas, carros e igrejas, tudo estava desaparecendo
sem parar. E~as pessoas estavam assustadas, perplexas. Esse susto
súbito, essa perplexidade momentânea é o afeto permanente; é o lugar
natal do homem do buraco negro. Homens do buraco negro não entram em
crise ou em estado de pergunta ao constatarem uma rachadura na casa ou
um rasgão no uniforme. Não! Eles são uma crise permanente e uma questão
contínua; jamais vestiram uniforme ou aconchegaram"-se dentro de uma casa
ou lar que pudesse rachar. Eles já moram na fenda e na rachadura! Ali
onde o homem mundano escuta: ``É câncer, são 7 meses..:''; ali, para
onde ele olha por um segundo, por um triz, nessa fresta de horror, é lá
que o abismal esteve plantado a vida inteira. O~abismal é pai e avô do
câncer. Ele não precisa ter medo de morrer, pois já nasceu morto e
aposentado. Seu desassossego é o não poder instalar"-se no mundo, não
poder fixar residência no buraco branco. Quando um abismal, um fendido,
caminha à noite por uma cidade e olha para as luzes no interior das
casas e dos prédios, então ele está se perguntando: ``Mas como é
possível a vida? Como é possível deter"-se na faixa da presença, alcançar
essa dimensão e aí acontecer? Como é possível ter biografia? Seria
possível introduzir esse buraco (o desastre) na história e no tempo, sem
destruí"-los? Como é que eles não estão engolidos, por meio de que mágica
migram para a segurança do mundo e de um si"-mesmo? O que é, afinal, que
eu sei e que não deveria saber?'': Questões como essas revelam o segredo
dos abismais.

Muitos desses segredos foram acolhidos e anotados por Maurice Blanchot.
Ele saudou e honrou os abismais. Mostrou a dignidade da notícia que
carregam. Para Blanchot, o excluído, ao dizer o mundo de seu tempo a
partir do buraco, isto é, a partir do outro de qualquer mundo, protege o
humano de uma absorção definitiva pelo buraco branco. A~lonjura e a fuga
permanente da palavra literária impedem qualquer fixação na forma"-mundo,
asseguram a vida insegura.

Essa topologia não pretende ontologizar as regiões. Tudo é passagem e
tudo é trânsito. Se o homem do buraco branco pode ser devidamente
estuprado e perder a medida do seu fundamento para, então, adentrar num
devir"-inconsciente e vagabundear na desmesura da amizade, também o homem
do buraco negro espera o ``sim'' que o conduza até o fluxo arrebatado.
Não é o ``sim'' do buraco branco; contra este, ele já está imunizado; é
contra ele que gritou e escreveu, e foi por ter encontrado apenas ele
que ficou interrompido. Mais dia, menos dia, ele pode confiar em algum
convite. Não me refiro ao convite ocular dos filósofos brancos: ``Veja o
rio, pense o rio'': mas ao convite da mulher gestual e terrena: ``Ponha
o pé no rio, ponha as mãos na água'': Afinal, o humano não pertence nem
só ao buraco onde não aparece e nem ao instituído onde desaparece. Entre
o ser domado e o não ser se abre a região comovida do agradecimento e do
encontro. ``Queria ser um cometa? Acredito que sim. Cometas têm a
velocidade dos pássaros, florescem ao fogo e na pureza são como
crianças''. É~um verso de Hõlderlin, ele ensina que a incandescência nos
aguarda… sempre ---:

Para cada ``mundo'' existe um antimundo e um contramundo. Para todo
não"-mundo, uma passagem. Implosão e explosão contínuas, instabilidade
perpétua.

\section{\versal{CITAÇ}Õ\versal{ES}}

\emph{Uma frase de:}

\begin{itemize}
\item
  \versal{LAWRENCE}, D.H\,\emph{Apocalipse seguido} de \emph{O~Homem que Morreu}.
  São Paulo, Companhia das Letras, 1990.
\end{itemize}
\emph{Uma palavra de:}

\begin{itemize}
\item
  \versal{BERDIAEV}, N\,\emph{De la Destination de l'homme. Essai d'ethique
  Paradoxale}. Paris, L'Age Homme, 1979.
\end{itemize}
